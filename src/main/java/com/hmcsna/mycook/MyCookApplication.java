package com.hmcsna.mycook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyCookApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyCookApplication.class, args);
    }

}
