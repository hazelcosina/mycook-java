package com.hmcsna.mycook.controller;

import com.hmcsna.mycook.entity.Cart;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @CrossOrigin(origins = {"http://localhost:3000", "https://mycook-app.herokuapp.com"})
    @PostMapping("/new")
    public Cart newEmployee(@RequestBody Cart cart) {
        System.out.println(cart.toString());
        return cart;
    }
}
