package com.hmcsna.mycook.controller;

import com.hmcsna.mycook.entity.Option;
import com.hmcsna.mycook.entity.Product;
import com.hmcsna.mycook.entity.Shop;
import com.hmcsna.mycook.entity.Variation;
import com.hmcsna.mycook.repository.OptionRepository;
import com.hmcsna.mycook.repository.ProductRepository;
import com.hmcsna.mycook.repository.ShopRepository;
import com.hmcsna.mycook.repository.VariationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/shops")
public class SearchController {

    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private VariationRepository variationRepository;
    @Autowired
    private OptionRepository optionRepository;

//    @GetMapping("/test")
    public List<Shop> test() {

        Shop shop1 = new Shop();
        shop1.setShopName("Cookie Monster");
        shop1.setAddress("14 Don Carlos Revilla St");
        shop1.setCity("Pasay");
        shop1.setDescription("Made from premium ingredients. Our customized fondant cakes are baked and designed meticulously to ensure freshness and quality.");
        shop1.setCategory("Desserts, Sweets, Cakes");
        shop1.setPostalCode("13001");
        shop1.setEmailAddress("");
        shop1.setRestaurantImage("assets/img/examples/shop-14/sample1.jpg");

        Shop shop2 = new Shop();
        shop2.setShopName("Sushi Tasty");
        shop2.setAddress("145 Villamor St");
        shop2.setCity("Makati");
        shop2.setDescription("Home made firewood pizza flavor of your choice.");
        shop2.setCategory("Japanese");
        shop2.setPostalCode("13001");
        shop2.setEmailAddress("");
        shop2.setRestaurantImage("assets/img/examples/shop-15/sample1.jpg");

        // Shop 1
        List <Product> items = new ArrayList<>();
        Product cake = new Product();
        cake.setName("Cookie1");
        cake.setDescription("Oatmeal cookie fresh from oven");
        cake.setImage1("assets/img/examples/shop-14/sample2.jpg");
        cake.setPrice(new BigDecimal(1000.00));
        items.add(cake);

        Product item2 = new Product();
        item2.setName("Cookie2");
        item2.setDescription("Hazelnut cookie flavor");
        item2.setImage1("assets/img/examples/shop-14/sample3.jpg");
        item2.setPrice(new BigDecimal(500.00));
        items.add(item2);

        List <Product> shop2items = new ArrayList<>();
        Product shop2item1 = new Product();
        shop2item1.setName("Sushi1");
        shop2item1.setDescription("A4 Cheese pizza firewood-made available on 3 sizes. Up to 24 inches!");
        shop2item1.setImage1("assets/img/examples/shop-15/sample2.jpg");
        shop2item1.setPrice(new BigDecimal(800.00));
        shop2items.add(shop2item1);


        Product shop2item2 = new Product();
        shop2item2.setName("Sushi2");
        shop2item2.setDescription("Aglio olio Spicy");
        shop2item2.setImage1("assets/img/examples/shop-15/sample3.jpg");
        shop2item2.setPrice(new BigDecimal(500.00));
        shop2items.add(item2);

//        // Create variation for above product "Cake"
//        List<Variation> productVariations = new ArrayList<>();
//
//        // Layers
//        Variation layers = new Variation();
//        layers.setVariationName("Layers");
//        layers.setType(1); //1 is radio, 2 for checkbox, 3 for text area, 4 dropdown
//
//        List<Option> layerOptions = new ArrayList<>();
//        Option layer1 = new Option();
//        layer1.setOptionName("1");
//        layer1.setPrice(new BigDecimal(0.00));
//        layerOptions.add(layer1);
//
//        Option layer2 = new Option();
//        layer2.setOptionName("2");
//        layer2.setPrice(new BigDecimal(200.00));
//        layerOptions.add(layer2);
//
//        Option layer3 = new Option();
//        layer3.setOptionName("3");
//        layer3.setPrice(new BigDecimal(500.00));
//        layerOptions.add(layer3);
//
//        optionRepository.save(layer1);
//        optionRepository.save(layer2);
//        optionRepository.save(layer3);
//
//        layers.setOptionList(layerOptions);
//
//        variationRepository.save(layers);
//
//        // Flavor
//        Variation flavor = new Variation();
//        flavor.setVariationName("Flavor");
//        flavor.setType(1);
//
//        List<Option> flavorOptions = new ArrayList<>();
//        Option flavor1 = new Option();
//        flavor1.setOptionName("Vanilla");
//        flavor1.setPrice(new BigDecimal(0.00));
//        flavorOptions.add(flavor1);
//
//        Option flavor2 = new Option();
//        flavor2.setOptionName("Chocolate");
//        flavor2.setPrice(new BigDecimal(5.00));
//
//        flavorOptions.add(flavor2);
//
//        optionRepository.save(flavor1);
//        optionRepository.save(flavor2);
//
//        flavor.setOptionList(flavorOptions);
//
//        variationRepository.save(flavor);
//
//        productVariations.add(flavor);
//        productVariations.add(layers);
//
//        cake.setVariationList(productVariations);

        productRepository.save(cake);
        productRepository.save(item2);
        productRepository.save(shop2item1);
        productRepository.save(shop2item2);

        shop1.setMenuItemList(items);
        shop2.setMenuItemList(shop2items);

        shopRepository.save(shop1);
        shopRepository.save(shop2);

        return null;
    }

//    @GetMapping("/{location}")
//    public List<Shop> getShopsByCity(@PathVariable(value = "city") String city) {
//        return this.shopRepository.findByCity(city);
//    }

    @GetMapping("/all")
    public List<Shop> getAllShops() {
        return this.shopRepository.findAll();
    }

}
