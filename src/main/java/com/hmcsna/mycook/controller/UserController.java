package com.hmcsna.mycook.controller;

import com.hmcsna.mycook.entity.AuthResponse;
import com.hmcsna.mycook.entity.MyUserDetails;
import com.hmcsna.mycook.entity.User;
import com.hmcsna.mycook.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/get")
    public ResponseEntity<User> getUserDetails(String username) {
        MyUserDetails user = userService.loadUserByUsername(username);
        return new ResponseEntity<>(user.getUser(), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<User> createUser(@RequestBody User user) {
        try {
            userService.save(user);
            return new ResponseEntity<>(user, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}