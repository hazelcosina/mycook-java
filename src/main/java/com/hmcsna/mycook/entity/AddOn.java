package com.hmcsna.mycook.entity;

public class AddOn {
    private long addOnId;
    private Variation variation;
    private Option option;

    public long getAddOnId() {
        return addOnId;
    }

    public void setAddOnId(long addOnId) {
        this.addOnId = addOnId;
    }

    public Variation getVariation() {
        return variation;
    }

    public void setVariation(Variation variation) {
        this.variation = variation;
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }

    @Override
    public String toString() {
        return "AddOn{" +
                "addOnId=" + addOnId +
                ", variation=" + variation +
                ", option=" + option +
                '}';
    }
}
