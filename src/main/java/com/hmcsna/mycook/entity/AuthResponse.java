package com.hmcsna.mycook.entity;

public class AuthResponse {

    private User user;
    private String response;

    public AuthResponse(User user, String response) {
        this.user = user;
        this.response = response;
    }
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
