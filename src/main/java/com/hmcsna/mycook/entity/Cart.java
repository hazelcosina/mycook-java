package com.hmcsna.mycook.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Cart {

    private long cartId;
    private List<CartItem> cartItems;
    private BigDecimal totalPayment;

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    public BigDecimal getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(BigDecimal totalPayment) {
        this.totalPayment = totalPayment;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "cartId=" + cartId +
                ", cartItems=" + cartItems +
                ", totalPayment=" + totalPayment +
                '}';
    }
}
