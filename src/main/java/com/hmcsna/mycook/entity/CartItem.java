package com.hmcsna.mycook.entity;

import java.math.BigDecimal;

public class CartItem {

    private long cartItemId;
    private long productId;
    private String name;
    private BigDecimal price;
    private long shopId;
    private int quantity;
    private BigDecimal addOnsPrice;
    private AddOn addOn1;
    private AddOn addOn2;
    private AddOn addOn3;
    private AddOn addOn4;
    private AddOn addOn5;

    public long getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(long cartItemId) {
        this.cartItemId = cartItemId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAddOnsPrice() {
        return addOnsPrice;
    }

    public void setAddOnsPrice(BigDecimal addOnsPrice) {
        this.addOnsPrice = addOnsPrice;
    }

    public AddOn getAddOn1() {
        return addOn1;
    }

    public void setAddOn1(AddOn addOn1) {
        this.addOn1 = addOn1;
    }

    public AddOn getAddOn2() {
        return addOn2;
    }

    public void setAddOn2(AddOn addOn2) {
        this.addOn2 = addOn2;
    }

    public AddOn getAddOn3() {
        return addOn3;
    }

    public void setAddOn3(AddOn addOn3) {
        this.addOn3 = addOn3;
    }

    public AddOn getAddOn4() {
        return addOn4;
    }

    public void setAddOn4(AddOn addOn4) {
        this.addOn4 = addOn4;
    }

    public AddOn getAddOn5() {
        return addOn5;
    }

    public void setAddOn5(AddOn addOn5) {
        this.addOn5 = addOn5;
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "cartItemId=" + cartItemId +
                ", productId=" + productId +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", shopId=" + shopId +
                ", quantity=" + quantity +
                ", addOnsPrice=" + addOnsPrice +
                ", addOn1=" + addOn1 +
                ", addOn2=" + addOn2 +
                ", addOn3=" + addOn3 +
                ", addOn4=" + addOn4 +
                ", addOn5=" + addOn5 +
                '}';
    }
}
