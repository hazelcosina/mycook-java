package com.hmcsna.mycook.entity;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class Order {

    private long orderId;
    private List<Product> orderedProducts;
    private CustomerReview review;
    private User user;
    private boolean hasReview;
    private boolean isActive;
    private boolean isCancelled;
    private Timestamp timestamp;
    private int discount;
    private Date pickupDate;
    private int pickupTime;
    private String voucherCode;
}
