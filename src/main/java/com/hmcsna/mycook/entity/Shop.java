package com.hmcsna.mycook.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_shop")
public class Shop {

    @Id
    @GeneratedValue
    @Column(name = "shop_id")
    private long shopId;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "shop")
    private List<Product> productList;
    private String shopName;
    private String address;
    private String city;
    private String postalCode;
    private String state;
    private String emailAddress;
    private String category;
    private String description;
    private String restaurantImage;
    private Double latitude;
    private Double longitude;


    public Shop() {
    }

    public Shop(long shopId, List<Product> productList, String shopName, String address, String city, String postalCode, String state, String emailAddress, String category, String description, String restaurantImage, Double latitude, Double longitude) {
        this.shopId = shopId;
        this.productList = productList;
        this.shopName = shopName;
        this.address = address;
        this.city = city;
        this.postalCode = postalCode;
        this.state = state;
        this.emailAddress = emailAddress;
        this.category = category;
        this.description = description;
        this.restaurantImage = restaurantImage;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRestaurantImage() {
        return restaurantImage;
    }

    public void setRestaurantImage(String restaurantImage) {
        this.restaurantImage = restaurantImage;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @JsonManagedReference
    public List<Product> getProductList() {
        return productList;
    }

    public void setMenuItemList(List<Product> productList) {
        this.productList = productList;
    }

}
