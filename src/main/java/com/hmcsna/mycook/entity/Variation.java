package com.hmcsna.mycook.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_variation")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "variationId")
public class Variation {

    @Id
    @GeneratedValue
    private long variationId;
    private String variationName;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,mappedBy = "variation")
    private List<Option> optionList;
    private int type;

    public long getVariationId() {
        return variationId;
    }

    public void setVariationId(long variationId) {
        this.variationId = variationId;
    }

    public String getVariationName() {
        return variationName;
    }

    public void setVariationName(String variationName) {
        this.variationName = variationName;
    }

    @JsonManagedReference
    public List<Option> getOptionList() {
        return optionList;
    }

    public void setOptionList(List<Option> optionList) {
        this.optionList = optionList;
    }

    @JsonBackReference
    public Product getProduct() {  return product; }

    public void setProduct(Product product) { this.product = product;  }

    public int getType() {  return type;  }

    public void setType(int type) { this.type = type;  }

    @Override
    public String toString() {
        return "Variation{" +
                "variationId=" + variationId +
                ", variationName='" + variationName + '\'' +
                ", product=" + product +
                ", optionList=" + optionList +
                ", type=" + type +
                '}';
    }
}
