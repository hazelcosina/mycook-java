package com.hmcsna.mycook.filters;

import com.hmcsna.mycook.entity.User;

import java.io.Serializable;

public class JwtResponse implements Serializable
{
    private final String jwttoken;

    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }

    public String getToken() {
        return this.jwttoken;
    }

}