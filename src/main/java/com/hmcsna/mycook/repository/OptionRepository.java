package com.hmcsna.mycook.repository;

import com.hmcsna.mycook.entity.Option;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OptionRepository extends JpaRepository<Option, Long> {
}
