package com.hmcsna.mycook.repository;

import com.hmcsna.mycook.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
