package com.hmcsna.mycook.repository;

import com.hmcsna.mycook.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmailAddress(String emailAddress);
    User findByContactNumber(String contactNumber);
    User findByUsername(String username);
}
