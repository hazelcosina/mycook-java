package com.hmcsna.mycook.repository;

import com.hmcsna.mycook.entity.Shop;
import com.hmcsna.mycook.entity.Variation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VariationRepository extends JpaRepository<Variation, Long>  {
}
