package com.hmcsna.mycook.service;

import com.hmcsna.mycook.entity.MyUserDetails;
import com.hmcsna.mycook.entity.User;
import com.hmcsna.mycook.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public MyUserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {

        User user = userRepository.findByUsername(username);
        if (user != null)
        {
            return new MyUserDetails(user);

//            return new User("admin", "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6",
//                    new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }

    public void save(User user){
        if(StringUtils.isEmpty(user.getUsername() )){
            user.setUsername(user.getEmailAddress());
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setRole("user");
        }
        userRepository.save(user);
    }
}